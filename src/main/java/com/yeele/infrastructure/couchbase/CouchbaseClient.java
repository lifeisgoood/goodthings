package com.yeele.infrastructure.couchbase;


import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import rx.Observable;
import rx.functions.Func1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by makoto.kitayama on 7/3/17.
 */
public class CouchbaseClient {
    protected CouchbaseCluster cluster;
    protected Bucket bucket;
    public CouchbaseClient(String host, String bucketName) {

        // Create a cluster reference
        cluster = CouchbaseCluster.create(host);
        // Connect to the bucket and open it
        bucket = cluster.openBucket(bucketName);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            super.finalize();
        } finally {
            destruction();
        }
    }

    private void destruction() {
        if(cluster != null){
            cluster.disconnect();
        }
    }

    public Bucket getBucket() {
        return bucket;
    }

    public List<JsonDocument> bulkGet(List<String> keys){
        int N = 10;
        List<JsonDocument> docs = new ArrayList<>();
        while(keys.size() >= 10) {
            List<String> subkeys = keys.subList(keys.size() - N, keys.size());
            List<JsonDocument> foundDocs = Observable
                    .just(subkeys.get(0), subkeys.get(1), subkeys.get(2), subkeys.get(3),
                            subkeys.get(4), subkeys.get(5), subkeys.get(6), subkeys.get(7),
                            subkeys.get(8), subkeys.get(9))
                    .flatMap(new Func1<String, Observable<JsonDocument>>() {
                        @Override
                        public Observable<JsonDocument> call(String id) {
                            return bucket.async().get(id);
                        }
                    })
                    .toList()
                    .toBlocking()
                    .single();
            docs.addAll(foundDocs);
            subkeys.clear();
        }
        keys.forEach(new Consumer<String>() {
            @Override
            public void accept(String id) {
                JsonDocument doc = bucket.get(id);
                if(doc != null){
                    docs.add(doc);
                }
            }
        });
        return docs;
    }
    public void sample() {
        // Create a JSON document and store it with the ID "helloworld"
        JsonObject content = JsonObject.create().put("hello", "world");
        JsonDocument inserted = bucket.upsert(JsonDocument.create("helloworld", content));

        // Read the document and print the "hello" field
        JsonDocument found = bucket.get("helloworld");
        System.out.println("Couchbase is the best database in the " + found.content().getString("hello"));
    }
}
