package com.yeele.goodthings.application;

import com.yeele.goodthings.domain.resource.User;
import com.yeele.goodthings.domain.resource.response.Body;
import com.yeele.goodthings.domain.resource.response.GoodThingResponse;
import com.yeele.goodthings.domain.resource.response.Status;
import com.yeele.goodthings.domain.resource.response.UserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by makoto.kitayama on 7/16/17.
 */

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController extends CouchbaseController {

    private static Logger logger = LoggerFactory.getLogger(GoodThingController.class);

    public UserController(){
        super();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GoodThingResponse register(@RequestBody User user){
        logger.debug("login controller register start ");
        logger.info(String.format("user id: %s, credential: %s", user.getId(), user.getCredential()));

        GoodThingResponse resp = null;
        User u = gtConn.findUserById(user.getId());
        if (u != null){
            resp = new GoodThingResponse(new Status("ok", "already exists"));
        }else{
            gtConn.upsertUser(user);
            resp = new GoodThingResponse(new Status("ok", "created"));
        }

        // Todo: return cookie? what is needed for SP native app?
        return resp;
    }

    @RequestMapping(path="/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GoodThingResponse login(@RequestBody User user){
        logger.debug("login controller login start ");
        logger.info(String.format("user: %s", user));

        GoodThingResponse resp = null;
        User u = gtConn.findUserById(user.getId());
        if (u == null){
            resp = new GoodThingResponse(new Status("notfound", "user doesn't exist"));
        }else{
            // check credintial
            if(u.getCredential().equals(user.getCredential())){
                resp = new GoodThingResponse(new Status("ok", "successfuly login"));
            }else{
                resp = new GoodThingResponse(new Status("error", "user/credential invalid"));
            }
        }
        return resp;

    }

    @RequestMapping(value="/{userId:.+}", method = RequestMethod.GET)
    public UserResponse getUser(@PathVariable String userId){
        logger.debug("login controller getUser start ");
        logger.info(String.format("userId: %s", userId));

        UserResponse resp = null;
        User u = gtConn.findUserById(userId);
        if (u == null){
            resp = new UserResponse(new Status("notfound", "user doesn't exist"));
        }else{
            resp = new UserResponse(new Status(), new Body(u));
        }
        return resp;
    }
}
