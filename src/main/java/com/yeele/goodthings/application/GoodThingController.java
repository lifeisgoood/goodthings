package com.yeele.goodthings.application;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.yeele.goodthings.domain.resource.GoodThing;
import com.yeele.goodthings.domain.resource.User;
import com.yeele.goodthings.domain.resource.UserHistory;
import com.yeele.goodthings.domain.resource.response.Body;
import com.yeele.goodthings.domain.resource.response.GoodThingResponse;
import com.yeele.goodthings.domain.resource.response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/goodthing")
@CrossOrigin(origins = "http://localhost:3000")
public class GoodThingController extends CouchbaseController {


    public GoodThingController() {
        super();
    }

    private static Logger log = LoggerFactory.getLogger(GoodThingController.class);

    @RequestMapping(path="/{id}", method = RequestMethod.GET)
    public GoodThingResponse getGoodthing(
            @PathVariable long id
    ) {
        GoodThingResponse resp = null;
        GoodThing gt;

        gt = gtConn.findGoodThingById(String.valueOf(id));
        if(gt != null){
            resp = new GoodThingResponse(new Status("ok"), new Body(gt));
        }else {
            resp = new GoodThingResponse(new Status("notfound"));
        }


        return resp;
    }

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public GoodThingResponse postGoodThing(@RequestBody GoodThing gt) {
        log.debug("POST calls postGoodThing");
        //GoodThing gt = new GoodThing("", "", "");
        gtConn.upsertGoodThing(gt);
        GoodThingResponse resp = new GoodThingResponse(new Status("ok"), new Body(gt));
        return resp;
    }

    @RequestMapping(path="/list", method = RequestMethod.GET)
    public GoodThingResponse getGoodthings(
            @RequestParam Map<String, String> params
    ) {
        GoodThingResponse resp = null;
        GoodThing gt;
        int limit = params.get("limit") != null ? Integer.parseInt(params.get("limit")) : 20;

        if (params.get("userId") != null) {
            String userId = params.get("userId");
            // get list of user history descing by modified date
            UserHistory userHistory = gtConn.findUserHistoryByUserId(userId);
            List<String> history = userHistory.getHistory();
            List<GoodThing> gts = gtConn.findGoodThingsByIds(history);
            resp = new GoodThingResponse(new Status(), new Body<GoodThing>(gts));
        }else {
            resp = new GoodThingResponse(new Status("error", "parameter missing"));
        }

        return resp;
    }
}
