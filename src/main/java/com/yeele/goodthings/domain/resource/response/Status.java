package com.yeele.goodthings.domain.resource.response;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class Status {
    private String code;
    private String message;

    public Status(){
        this("ok");
    }

    public Status(String code){
        this.code = code;
    }
    public Status(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
