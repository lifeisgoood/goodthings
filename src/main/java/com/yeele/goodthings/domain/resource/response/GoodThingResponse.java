package com.yeele.goodthings.domain.resource.response;

import com.yeele.goodthings.domain.resource.GoodThing;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class GoodThingResponse {
    private Status status;
    private Body<GoodThing> body;

    public GoodThingResponse(){}

    public GoodThingResponse(Status status){
        this.status = status;
    }

    public GoodThingResponse(Status status, Body<GoodThing> body){
        this.status = status;
        this.body = body;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Body<GoodThing> getBody() {
        return body;
    }

    public void setBody(Body<GoodThing> body) {
        this.body = body;
    }
}
