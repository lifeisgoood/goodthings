package com.yeele.goodthings.domain.resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class UserHistory {
    private String userId; // email
    private List<String> history; // list of keys for GoodThing e.g) gt_xxxxxx

    public UserHistory(String userId){
        this.userId = userId;
        history = new ArrayList<String>();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void addElement(String key){
        this.history.add(key);
    }

    public List<String> getHistory() {
        return history;
    }

    public void setHistory(List<String> history) {
        this.history = history;
    }
}
