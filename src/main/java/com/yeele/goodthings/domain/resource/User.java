package com.yeele.goodthings.domain.resource;

import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class User {
    @NotBlank
    @NotNull
    private String id; // email
    private String credential;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }
}
