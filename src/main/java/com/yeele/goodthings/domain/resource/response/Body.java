package com.yeele.goodthings.domain.resource.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class Body<T> {
    private List<T> items = new ArrayList<T>();

    public Body(){}

    public Body(T gt){
        this.items.add(gt);
    }
    public Body(List<T> gtList){
        this.items = gtList;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
