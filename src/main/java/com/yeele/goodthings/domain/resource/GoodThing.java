package com.yeele.goodthings.domain.resource;

/**
 * Created by makoto.kitayama on 6/17/17.
 */
public class GoodThing {
    private long id;
    private String content;
    private String userId; // <-- userId
    private String location; // <-- ToDo: Geo Location // <--- nice to visualize
    private long ts;

    public GoodThing(){
        this.ts = System.currentTimeMillis()/ 1000L;
    }

    public GoodThing(long id, String content, String user, String location){
        this.id = id;
        this.content = content;
        this.userId = user;
        this.location = location;
        this.ts = System.currentTimeMillis()/ 1000L;
    }

    public GoodThing(String content, String userId, String location){
        this.content = content;
        this.userId = userId;
        this.location = location;
        this.ts = System.currentTimeMillis()/ 1000L;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getUserId() {
        return userId;
    }

    public String getLocation() {
        return location;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }
}
