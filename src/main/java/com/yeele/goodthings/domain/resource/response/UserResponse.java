package com.yeele.goodthings.domain.resource.response;

import com.yeele.goodthings.domain.resource.User;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class UserResponse {
    private Status status;
    private Body<User> body;

    public UserResponse(){}

    public UserResponse(Status status){
        this.status = status;
    }

    public UserResponse(Status status, Body<User> body){
        this.status = status;
        this.body = body;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Body<User> getBody() {
        return body;
    }

    public void setBody(Body<User> body) {
        this.body = body;
    }
}
