package com.yeele.goodthings.infrastructure.elasticsearch;

import com.yeele.goodthings.domain.resource.GoodThing;
import com.yeele.goodthings.domain.resource.User;
import com.yeele.goodthings.domain.resource.UserHistory;
import com.yeele.goodthings.infrastructure.GoodThingsDBIF;
import com.yeele.infrastructure.elasticsearch.ElasticSearchClient;

import java.util.List;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class GoodThingElasticSearchClient extends ElasticSearchClient
implements GoodThingsDBIF {
    @Override
    public void connect() {

    }

    @Override
    public User findUserById(String userId) {
        return null;
    }

    @Override
    public List<GoodThing> findGoodThingsByIds(List<String> ids) {
        return null;
    }

    @Override
    public boolean upsertUser(User user) {
        return false;
    }

    @Override
    public GoodThing findGoodThingById(String id) {
        return null;
    }

    @Override
    public UserHistory findUserHistoryByUserId(String userId) {
        return null;
    }

    @Override
    public boolean upsertGoodThing(GoodThing gt) {
        return false;
    }
}
