package com.yeele.goodthings.infrastructure.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.JsonLongDocument;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.google.gson.Gson;
import com.yeele.goodthings.application.GoodThingController;
import com.yeele.goodthings.domain.resource.GoodThing;
import com.yeele.goodthings.domain.resource.User;
import com.yeele.goodthings.domain.resource.UserHistory;
import com.yeele.goodthings.infrastructure.GoodThingsDBIF;
import com.yeele.infrastructure.couchbase.CouchbaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public class GoodThingCouchbaseClient extends CouchbaseClient
        implements GoodThingsDBIF {

    private static Logger logger = LoggerFactory.getLogger(GoodThingCouchbaseClient.class);
    public static final String KEY_PREFIX_USER = "u_";
    public static final String KEY_PREFIX_USER_HIST = "uh_";
    public static final String KEY_PREFIX_GT = "gt_";
    protected Gson gson;
    private Broker<GoodThing> brokerGoodThing = new Broker<GoodThing>(KEY_PREFIX_GT, GoodThing.class);
    private Broker<User> brokerUser = new Broker<User>(KEY_PREFIX_USER, User.class);
    private Broker<UserHistory> brokerUserHistory = new Broker<UserHistory>(KEY_PREFIX_USER_HIST, UserHistory.class);

    public GoodThingCouchbaseClient() {
        super("127.0.0.1", "goodthings");
        gson = new Gson();
        connect();
    }

    @Override
    public void connect() {

    }

    // use generics
    private class Broker<T> {
        private String prefix = "";
        private Class<T> type;

        public Broker(String prefix, Class<T> cls) {
            this.prefix = prefix;
            this.type = cls;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public Class<T> getType() {
            return type;
        }

        public void setType(Class<T> type) {
            this.type = type;
        }

        public T docToObject(JsonDocument doc) {
            T obj = null;
            if (doc != null) {
                JsonObject content = doc.content();
                obj = (T) gson.fromJson(content.toString(), this.type);
            }
            return obj;
        }

        public T findById(String id) {
            T obj = null;
            JsonDocument doc = bucket.get(prefix + id);
            return docToObject(doc);
        }

        public List<T> findByIds(List<String> ids) {
            T obj = null;
            for (int i = 0; i < ids.size(); i++) {
                ids.set(i, prefix + ids.get(i));
            }
            List<JsonDocument> docs = bulkGet(ids);
            ArrayList<T> objs = new ArrayList<>();

            for (int i = 0; i < docs.size(); i++) {
                obj = docToObject(docs.get(i));
                if (obj != null) {
                    objs.add(obj);
                }
            }
            return objs;
        }

        public void upsert(String id, T obj) {
            RawJsonDocument gtDoc = RawJsonDocument.create(id, gson.toJson(obj));
            bucket.upsert(gtDoc);
        }

        public void upsertWithPrefix(String id, T obj) {
            RawJsonDocument gtDoc = RawJsonDocument.create(prefix + id, gson.toJson(obj));
            bucket.upsert(gtDoc);
        }
    }

    @Override
    public GoodThing findGoodThingById(String id) {
        return brokerGoodThing.findById(id);
    }

    public List<GoodThing> findGoodThingsByIds(List<String> ids) {
        return brokerGoodThing.findByIds(ids);
    }

    @Override
    public User findUserById(String id) {
        return brokerUser.findById(id);
    }

    @Override
    public UserHistory findUserHistoryByUserId(String userId) {
        return brokerUserHistory.findById(userId);
    }

    @Override
    public boolean upsertGoodThing(GoodThing gt) {
        long gtAutoIncId = bucket.counter(KEY_PREFIX_GT, 1, 0).cas();
        gt.setId(gtAutoIncId);
        brokerGoodThing.upsertWithPrefix(String.valueOf(gt.getId()), gt);

        UserHistory userHistory = brokerUserHistory.findById(gt.getUserId());
        if (userHistory == null) {
            logger.debug("instanciate UserHistory for " + gt.getUserId());
            userHistory = new UserHistory(gt.getUserId());
        }
        userHistory.addElement(String.valueOf(gt.getId()));
        brokerUserHistory.upsertWithPrefix(userHistory.getUserId(), userHistory);

        return true;
    }

    @Override
    public boolean upsertUser(User user) {
        brokerUser.upsert(String.valueOf(KEY_PREFIX_USER + user.getId()), user);

        upsertUserHistory(new UserHistory(user.getId()));
        return true;
    }

    public boolean upsertUserHistory(UserHistory userHistory) {
        brokerUserHistory.upsert(String.valueOf(KEY_PREFIX_USER_HIST + userHistory.getUserId()), userHistory);
        return true;
    }
}
