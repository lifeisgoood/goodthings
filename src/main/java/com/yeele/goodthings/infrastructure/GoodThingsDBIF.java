package com.yeele.goodthings.infrastructure;

import com.yeele.goodthings.domain.resource.GoodThing;
import com.yeele.goodthings.domain.resource.User;
import com.yeele.goodthings.domain.resource.UserHistory;

import java.util.List;

/**
 * Created by makoto.kitayama on 7/16/17.
 */
public interface GoodThingsDBIF {

    public void connect();
    // GET
    public GoodThing findGoodThingById(String id);
    public List<GoodThing> findGoodThingsByIds(List<String> ids);
    public User findUserById(String userId);
    public UserHistory findUserHistoryByUserId(String userId);
    //public List<GoodThing> findGoodThingById(List<String> ids);

    // SET
    public boolean upsertGoodThing(GoodThing gt);
    public boolean upsertUser(User user);
}
